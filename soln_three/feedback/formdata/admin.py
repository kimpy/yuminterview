from django.contrib import admin

from models import Neighbourhood,Feedback
from actions import export_as_csv_action

# Register your models here.
@admin.register(Neighbourhood)
class NeighbourhoodAdmin(admin.ModelAdmin):
	list_display = ['name','slug']
	prepopulated_fields =  {'slug': ('name',)}

@admin.register(Feedback)
class Feedback(admin.ModelAdmin):

	list_display = ['name','phone_no','neighbourhood','rating','comments','created']
	actions = [export_as_csv_action("CSV Export", fields=['name','phone_no','neighbourhood','rating','comments','created'])]