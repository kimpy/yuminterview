from django.shortcuts import render

# Create your views here.
def index(request):

	template = "landing/home.html"
	context = {}

	return render(request,template,context)

def register(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            from django.contrib.auth import authenticate, login
            user = authenticate(username=form.cleaned_data.get('username'), password=form.cleaned_data.get('password1'))
            login(request, user)
            return HttpResponseRedirect(reverse('index'))
    else:
        form = UserCreationForm()
        
    return render_to_response('registration/register.html', {
        'form': form,
        }, context_instance=RequestContext(request))