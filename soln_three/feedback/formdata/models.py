from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Neighbourhood(models.Model):
	slug = models.SlugField(max_length=10,unique=True)
	name = models.CharField(max_length=20, blank = False)

	def __unicode__(self):
		return self.slug


class Feedback(models.Model):

	STARS = (('ONE',1),
			('TWO',2),
			('THREE',3),
			('FOUR',4),
			('FIVE',5)
			)

	name = models.ForeignKey(User, related_name='comments')
	phone_no = models.CharField(max_length = 200,)
	neighbourhood = models.ForeignKey(Neighbourhood, related_name='hoods')
	rating = models.CharField(max_length=10, choices = STARS)
	comments = models.TextField()
	created = models.DateTimeField(auto_now_add = True)

	def __unicode__(self):
		return self.name.first_name
