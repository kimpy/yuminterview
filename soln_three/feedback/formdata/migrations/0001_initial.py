# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Feedback',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('phone_no', models.CharField(max_length=200)),
                ('rating', models.CharField(max_length=10, choices=[(b'ONE', 1), (b'TWO', 2), (b'THREE', 3), (b'FOUR', 4), (b'FIVE', 5)])),
                ('comments', models.TextField()),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('name', models.ForeignKey(related_name='comments', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Neighbourhood',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('slug', models.SlugField(unique=True, max_length=10)),
                ('name', models.CharField(max_length=20)),
            ],
        ),
        migrations.AddField(
            model_name='feedback',
            name='neighbourhood',
            field=models.ForeignKey(related_name='hoods', to='formdata.Neighbourhood'),
        ),
    ]
