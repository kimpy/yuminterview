import csv

class FarmAnalytics(object):
	"""docstring for FarmAnalytics"""
	def __init__(self, path_to_data):		
		self.path_to_data = path_to_data

	def biomass_max_and_min(self):

		with open(self.path_to_data,'rb') as farm_data:
			read_data = csv.reader(farm_data)			
			next(read_data)
			data_list = list(read_data) # skip the first row
		max_row = max(data_list,key=lambda column:column[11])						
		min_row = min(data_list, key = lambda column:column[11])	
			
		return {'Highest Biomass':max_row[0],
					'Lowest Biomass':min_row[0]			
				}			


	def sort_harvest_date(self):
		with open(self.path_to_data,'rb') as farm_data:
			data = csv.reader(farm_data)
			next(data)
			finaldt = sorted(data, key = lambda column:column[7])
			return finaldt

	def average_plants_harvest(self):
		with open(self.path_to_data,'rb') as farm_data:
			data = csv.reader(farm_data)
			next(data)
			harverst_list= [float(harvest[10]) for harvest in data]			
		return sum(harverst_list)/len(harverst_list)


if __name__ == '__main__':

	sample = FarmAnalytics('sample_data_for_test.csv')
	
	#test by printing
	print sample.biomass_max_and_min()
	print sample.sort_harvest_date()
	print sample.average_plants_harvest()
