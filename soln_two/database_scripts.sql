--Create database farmdata
CREATE DATABASE farm_data
  WITH OWNER = postgres
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       LC_COLLATE = 'en_US.UTF-8'
       LC_CTYPE = 'en_US.UTF-8'
       CONNECTION LIMIT = 100;

COMMENT ON DATABASE farm_data
  IS 'Farming data database for Yum test';

---Create table information
CREATE TABLE public.data
(
	id serial PRIMARY KEY,
   farmer_name character varying(200), 
   farm_mgnt_category character varying(10),    
   crop character varying(20), 
   variety character varying(20), 
   crop_sytem character varying(20), 
   plot_size character varying(10), 
   spacing character varying(10), 
   harversting_date date, 
   sampling_date date, 
   std_count_at_harvest integer, 
   number_of_harvest integer, 
   total_biomass numeric, 
   total_grain_fwt numeric, 
   total_stems_and_shell_pods numeric, 
   stems_and_shelled_pods_sample integer, 
   stems_and_shelled_pods_oven integer, 
   field_grain_sample integer, 
   field_grain_sample_oven integer, 
   total_stems_with_shelled_pods_oven numeric, 
   total_grain_oven numeric, 
   total_biomas_oven numeric
) 
WITH (
  OIDS = FALSE
)
;

--Data operations
--Get farmer  with highest total biomass
create view farmer_highest_biomass as
select farmer_name from data where total_biomass = ( select max(total_biomass) from data)

--Get farmer  with lowest total biomass
create view farmer_lowest_biomass as
select farmer_name from data where total_biomass = ( select min(total_biomass) from data)

--Sort the list by harvesting data
create view sorted_data as
select * from data order by harversting_date asc

--Average number of plant harvest
create view average_harvest as
select avg(number_of_harvest) from data